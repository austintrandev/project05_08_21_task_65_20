package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.devcamp.pizza365.model.*;
import com.devcamp.pizza365.repository.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class PizzaController {
	@Autowired
	IDrinkRepository pDrinkRepository;

	@GetMapping("/drinks")
	public ResponseEntity<List<CDrink>> getAllDrink() {
		try {
			List<CDrink> pDrinks = new ArrayList<CDrink>();
			pDrinkRepository.findAll().forEach(pDrinks::add);
			return new ResponseEntity<>(pDrinks, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Autowired
	IVoucherRepository pVoucherRepository;

	@GetMapping("/vouchers")
	public ResponseEntity<List<CVoucher>> getAllVoucher() {
		try {
			List<CVoucher> pVouchers = new ArrayList<CVoucher>();
			pVoucherRepository.findAll().forEach(pVouchers::add);
			return new ResponseEntity<>(pVouchers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Autowired
	IMenuRepository pMenuRepository;

	@GetMapping("/combomenu")
	public ResponseEntity<List<CMenu>> getAllMenu() {
		try {
			List<CMenu> pComboMenu = new ArrayList<CMenu>();
			pMenuRepository.findAll().forEach(pComboMenu::add);
			return new ResponseEntity<>(pComboMenu, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Autowired
	ICustomerRepository pCustomerRepository;

	@Autowired
	IOrderRepository pOrderRepository;

	@GetMapping("/users")
	public ResponseEntity<List<CCustomer>> getAllCustomer() {
		try {
			List<CCustomer> pCustomers = new ArrayList<CCustomer>();

			pCustomerRepository.findAll().forEach(pCustomers::add);

			return new ResponseEntity<>(pCustomers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/orders")
	public ResponseEntity<Set<COrder>> getOrdersByUserId(@RequestParam(value = "userId") String userId) {
		try {
			long vUserId = Long.parseLong(userId);
			CCustomer vCustomer = pCustomerRepository.findById(vUserId);

			if (vCustomer != null) {
				return new ResponseEntity<>(vCustomer.getOrders(), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
