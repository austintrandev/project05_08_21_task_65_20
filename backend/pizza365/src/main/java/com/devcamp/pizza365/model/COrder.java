package com.devcamp.pizza365.model;



import java.util.Date;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;

@Entity
@Table(name = "orders")
public class COrder {
	public COrder() {
		super();
	}

	public COrder(long id, String orderId, String kichCo, int duongKinh, int suon, int salad, String loaiPizza,
			String idVoucher, int thanhTien, int giamGia, String idLoaiNuocUong, int soLuongNuoc, String loiNhan,
			String trangThai, Date ngayTao, Date ngayCapNhat, CCustomer pCustomer) {
		super();
		this.id = id;
		this.orderId = orderId;
		this.kichCo = kichCo;
		this.duongKinh = duongKinh;
		this.suon = suon;
		this.salad = salad;
		this.loaiPizza = loaiPizza;
		this.idVoucher = idVoucher;
		this.thanhTien = thanhTien;
		this.giamGia = giamGia;
		this.idLoaiNuocUong = idLoaiNuocUong;
		this.soLuongNuoc = soLuongNuoc;
		this.loiNhan = loiNhan;
		this.trangThai = trangThai;
		this.ngayTao = ngayTao;
		this.ngayCapNhat = ngayCapNhat;
		this.pCustomer = pCustomer;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "order_id", unique = true)
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private String orderId;

	@Column(name = "kich_co")
	private String kichCo;
	
	@Column(name = "duong_kinh")
	private int duongKinh;
	
	@Column(name = "suon")
	private int suon;
	
	@Column(name = "salad")
	private int salad;
	
	@Column(name = "loai_pizza")
	private String loaiPizza;
	
	@Column(name = "id_voucher")
	private String idVoucher;
	
	@Column(name = "thanh_tien")
	private int thanhTien;
	
	@Column(name = "giam_gia")
	private int giamGia;
	
	@Column(name = "id_loai_nuoc_uong")
	private String idLoaiNuocUong;
	
	@Column(name = "so_luong_nuoc")
	private int soLuongNuoc;
	
	@Column(name = "loi_nhan")
	private String loiNhan;
	
	@Column(name = "trang_thai")
	private String trangThai;
	
	@Column(name = "ngay_tao")
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date ngayTao;
	
	@Column(name = "ngay_cap_nhat")
	@LastModifiedBy
	@Temporal(TemporalType.TIMESTAMP)
	private Date ngayCapNhat;
	
	@ManyToOne
    @JoinColumn(name="customer_id")
	CCustomer pCustomer;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public String getKichCo() {
		return kichCo;
	}

	public void setKichCo(String kichCo) {
		this.kichCo = kichCo;
	}

	public int getDuongKinh() {
		return duongKinh;
	}

	public void setDuongKinh(int duongKinh) {
		this.duongKinh = duongKinh;
	}

	public int getSuon() {
		return suon;
	}

	public void setSuon(int suon) {
		this.suon = suon;
	}

	public int getSalad() {
		return salad;
	}

	public void setSalad(int salad) {
		this.salad = salad;
	}

	public String getLoaiPizza() {
		return loaiPizza;
	}

	public void setLoaiPizza(String loaiPizza) {
		this.loaiPizza = loaiPizza;
	}

	public String getIdVoucher() {
		return idVoucher;
	}

	public void setIdVoucher(String idVoucher) {
		this.idVoucher = idVoucher;
	}

	public int getThanhTien() {
		return thanhTien;
	}

	public void setThanhTien(int thanhTien) {
		this.thanhTien = thanhTien;
	}

	public int getGiamGia() {
		return giamGia;
	}

	public void setGiamGia(int giamGia) {
		this.giamGia = giamGia;
	}

	public String getIdLoaiNuocUong() {
		return idLoaiNuocUong;
	}

	public void setIdLoaiNuocUong(String idLoaiNuocUong) {
		this.idLoaiNuocUong = idLoaiNuocUong;
	}

	public int getSoLuongNuoc() {
		return soLuongNuoc;
	}

	public void setSoLuongNuoc(int soLuongNuoc) {
		this.soLuongNuoc = soLuongNuoc;
	}

	public String getLoiNhan() {
		return loiNhan;
	}

	public void setLoiNhan(String loiNhan) {
		this.loiNhan = loiNhan;
	}

	public String getTrangThai() {
		return trangThai;
	}

	public void setTrangThai(String trangThai) {
		this.trangThai = trangThai;
	}

	public Date getNgayTao() {
		return ngayTao;
	}

	public void setNgayTao(Date ngayTao) {
		this.ngayTao = ngayTao;
	}

	public Date getNgayCapNhat() {
		return ngayCapNhat;
	}

	public void setNgayCapNhat(Date ngayCapNhat) {
		this.ngayCapNhat = ngayCapNhat;
	}

	public CCustomer getpCustomer() {
		return pCustomer;
	}

	public void setpCustomer(CCustomer pCustomer) {
		this.pCustomer = pCustomer;
	}

}